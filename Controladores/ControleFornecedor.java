package Controladores;

import Models.PessoaJuridica;
import Models.PessoaFisica;
import Models.Fornecedor;
import java.util.ArrayList;

public class ControleFornecedor {
    private  ArrayList<Fornecedor> ListaFornecedores;
     
    public ControleFornecedor(){
        ListaFornecedores = new ArrayList<Fornecedor>();
    }

    public ArrayList<Fornecedor> getListaFornecedores() {
        return ListaFornecedores;
    }

    public void setListaFornecedores(ArrayList<Fornecedor> ListaFornecedores) {
        this.ListaFornecedores = ListaFornecedores;
    }
     
     public String adicionar(PessoaFisica fornecedor){
         ListaFornecedores.add(fornecedor);
         return "Pessoa Fisica adcionada";
     }
     public String adicionar(PessoaJuridica fornecedor){
         ListaFornecedores.add(fornecedor);
         return "Pessoa Juridica adcionada";
     }
}
